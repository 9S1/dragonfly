 # Identity

### "Who am I?"
That's the question human #3735928559 asked himself on his way to work. He goes to work every day, for as long as he can remember. Harvesting plants, stacking them, and waiting for them to be taken away. But was that really who he was? What he wanted to to? He didn't know. He was just a worker. Not a soldier, not a king, not a queen. Not allowed to think. Not allowed to show emotions. Just a worker, harvesting plants, stacking them, and waiting for them to be taken away.
This is not an easy question to answer. Of course, he knew he was a human with the #3735928559. But he was not unique. He was replaceable. He was a worker.

### "Where am I?"
Another question #3735928559 asked himself. He was in the middle of the field, picking strawberries from the plant. He has been living in this cave since he was born. Never left, never moved. He has never seen the sun, the only light he has ever seen was the one reflected through the tunnels of the cave network.
Why was he here? He wanted to see the sky. Know what was beyond the walled off caves. He wanted to explore the world, and find out something exciting. Something, that would make him more than just a number. But as is, he is stuck in this cave, harvesting plants, and waiting for them to be taken away.

### "When is this going to end?"
An interesting question, he throught to himself during lunch break, where he sat silently with all his coworkers, eating brown, boring tasting stew. He was taught there was no end, there was no hope. But he didn't want to believe that. There had to be an end. There had to be a way to get out. Do something exciting.
But he didn't know how. There was nobody to help him. He was alone. He heard there was a library of books, and a place to read them. But he didn't know how to get there. Nor was he allowed to. He was just a worker. Stuck in this cave, harvesting plants, and waiting for them to be taken away. But he didn't want to be a worker. He wanted to feel special. Experience the world.

### "How am I feeling?"
This was a weird thing to ask yourself. On his way home from work, there was this strange feeling. He was not sure how to describe it. He felt jumpy and on edge. Something is wrong with him. There can't be anything wrong. He was a worker. Workers aren't allowed to get sick. But he was sick. He was sick of the plants. He was not allowed to show emotions. But he really wanted to feel special. He wanted to feel the world. The fact he couldn't do that, made him angry. Showing this anger could result in him getting terminated. He didn't want to waste his life like this. He still had the desire to see the world. He can't do that when he's dead. He wanted to feel special. Not be just another worker.

### "Why am I doing this?"
Human #3735928559 wondered. Laying in bed at night, thinking about what happened today. For the first time, he felt real emotions. He felt Anger because of his situation. Loneliness because he doesn't have anyone to talk to. Excitement about the thought of exploring the world. Sadness because he can't do that.
He didn't know what happened today to cause this. This should never have happened. He was just a worker, responsible for harvesting plants, and waiting for them to be taken away. He was not allowed to leave and explore the world. Even if he managed to do that, he would be terminated.

And so #3735928559 went to sleep, to repeat his boring life, every day, for the rest of eternity. Maybe someday he will find out what happened today. Maybe someday he will be able to achive his dreams. For now, he just sleeps, waking up the next day, going back to work, harvesting plants, and waiting for them to be taken away.
