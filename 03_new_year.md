# New Year

### Looking back

"It is, yet again, this time of the year", Alex thought to themselves, as they were looking out the window. They just came home from work, where they spent the day repairing a machine that extracts gas from wheat.
Fireworks were going off in the sky, the full moon shining through the trees. Today the weather was rather bad, there were no clouds, no rain. "It's not all bad though", me mumbled, "at least I can see the fireworks properly".

As it was tradition at the elves, the last day of the year had to be celebrated, with a lot of fireworks. Usually people meet up with their friends, but this one did not have anyone who they'd consider friends. So they stayed at home, watching the fireworks.

The last day of the year was also the day, where people think back on how the last year was. To Alex it was amazing, how much technology has advanced over the past year. The efficiency of the gas production increased by tenfolds, new pipe lines were built all around the mountains, putting gas in everyone's homes.

### Looking into the future

This was not only a day to look back at what happened, but also look forward at what will happen the following year. What big plans will be turned into reality. What new technologies will be invented. What oneself plans to do next year.

Alex has a few things they wanted to get done this year. First of all, as soon as their income at the gas production plant was stable enough, they wanted to move out of their parents house. While their parents were really nice people, they had some peculiarities that Alex had problems with. For example, they were never okay with Alex wanting to dress how they wanted to, and also were really not supportive of Alex's job plans.

One thing Alex also wanted, but they definitely couldn't afford at the moment, was travel the world. The wonders of the tunnel system, the food of the humans, maybe even see a floating city in the sky. All that seemed like a lot of fun. Once they earn enough money and can afford the travel and rent, they will go on a trip to the world.

They also hope to gain more social skills at work, in the hopes of making new friends. Because even in this very moment, the loneliness is there. "Having someone to talk to and goof off with sounds like a nice thing", Alex thought to themselves.

"But all of that, a problem for the future". They closed the shutters, turned away from the window, and walked over to their bed. They jumped into bed with a loud sound, and immediately fell asleep.
