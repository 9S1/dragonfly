# Worldbuilding

There once was a world, populated by three different races: Humans, elves and dragons.

## Inhabitants

### The Humans

The humans live near the west coast and do trading with the civilizations on the other side of the ocean. Above ground there is mostly desert, a not very inhabitable place. Because of that, humans have turned to living below the ground most of their lifes, in a giant cave network.

### The Elves

Elves live far away from the humans on the other side of the continent: The elves live in the mountains in the east. They are very intelligent and have a very good sense of smell. There is enough water and resources to survive, but it is rough at times. The further east you go, the colder the climate gets, and there is a line where it's so cold no person has ever gone there.
They are a self-sustaining civilization, mostly peaceful. Their fast healing results in them never being sick, as such they get much older than normal Humans.

### The Dragons

There are two types of dragons: The less powerful blue dragons, and the powerful, magic-adept red dragons. The blue dragons are rather small and held as pets by humans, and they can be very useful: Heating up the stove for cooking, or pulling the harvest to the ships to be sold off.
The red dragons are much bigger, elder dragons can reach the size of a mountain. With their ability to understand and use magic, they are very powerful, powerful enough to destroy entire civilizations. Not a lot is known about those dragons, they live in floating cities far above the ground, and only come down when hunting.
The lifespan on dragons depends on the magical capabilities they contain, which can be a few decades for blue dragons, for red dragons, a few hundreds or thousands of years is not unusual.

## Locations

### The Cave Network

There is a system of mirrors and lenses in place that redirect light from the outside to the inside.
In those caves, everything needed to survive is there: Plants are grown in the caves for Oxygen and food, there are caves with streams of water in them that are used for drinking. Being underground, the temperature is very stable, making their plants one of the tastiest in the entire world.

The cave network isn't build by the humans. There was an ancient civilization that went extinct thousands of years ago, that dug a giant cave network under the land. Not even a small portion of it has been discovered yet. It is said to go deep enough to reach hell itself, and is wide enough you can walk from coast to coast through them. Most of the cave system is walled off, there are many expeditions with the goal to create a map of the caves, but because of the massive size of the network this effort is still ongoing.

### The Mountains

The climate in the Mountains is quite harsh. In the lower mountains there are farms for animals and crops, this is where the elves get their food. The higher mountains are covered with thick snow, uninhabitable even for elves.

### The Floating Cities

The magic cities are kept afloat by an elder dragon. There are only a hand full of those cities, but they are large enough to contain hundreds of dragons each. They hover above the ground, the elder dragon can change the height and location of the city, and does so regularly. Less magic-adept dragons might not be able to levetate up to the city, but if the city isn't too high above the ground, they can fly up using their wings.

## The Conflict

Humans and red dragons are at war for hundreds of years. The red dragons see the pet dragons as mocking them, and humans need red dragon scales for the production of mirrors needed to light up the caves.
If a red dragon sees a human, they are taught to immediately attack. Because of this, the humans are taught to not leave their tunnels if there is a floating city nearby. And even if the humans want to attack the red dragons, they cannot. With the floating cities, there is no way to get up there without wings, so the red dragons are very safe there.
The humans are also rather safe from red dragons in their caves, because the entrances to the cave network are small enough to only fit human sized creatures, making it impossible for dragons to enter.

## Technical Advancements

In recent years, elves have come across a technology that would turn rotting crops into a foul smelling gas. It was discovered that this gas is highly flammable. Some of the elven cities have built a network of pipes to deliver this gas to the homes, where it can be used for cooking and heating. Some are experimenting with boiling water with this gas, and use the steam to open and close drawbridges.
This technology is not yet known to the other inhabitants of the world, but it is a very useful one. The elves are trying to create a way to make this gas now portable, so it can be carried around and maybe even be sold to humans.
